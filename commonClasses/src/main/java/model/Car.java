package model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Car implements Serializable {

    private static final long serialVersionUID = 1L;
    private int year;
    private int engineCapacity;
    private double purchasingPrice;

    @Builder
    public Car(int year, int engineCapacity, double purchasingPrice) {
        this.year = year;
        this.engineCapacity = engineCapacity;
        this.purchasingPrice = purchasingPrice;
    }

}