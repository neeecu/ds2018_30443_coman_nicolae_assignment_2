
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Car;
import remoteMethods.IPriceService;
import remoteMethods.ITaxService;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class App extends Application {

    //offsets used to make frame draggable
    private static double xOffset = 0;
    private static double yOffset = 0;

    public static void main(String[] args) throws RemoteException, NotBoundException {
        launch(args);
    }

    @Override
    public void start(Stage myStage) throws Exception {

        //load the log in stage
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ClientView.fxml"));
        Pane root =loader.load();

        //remove window borders
        myStage.initStyle(StageStyle.UNDECORATED);
        myStage.setTitle("Window Builder");

        root.setOnMousePressed(App::watchMouse);
        root.setOnMouseDragged(event->dragWindow(event,myStage));
        root.setOnMouseClicked(event->closeWindow(event,myStage));
        myStage.setScene(new Scene(root));
        myStage.show();

    }

    private static void watchMouse(MouseEvent event) {
        xOffset = event.getSceneX();
        yOffset = event.getSceneY();
    }

    private static void dragWindow(MouseEvent event, Stage myStage){
        myStage.setX(event.getScreenX() - xOffset);
        myStage.setY(event.getScreenY() - yOffset);
    }

    private static void closeWindow(MouseEvent event, Stage myStage){
        MouseButton pressedButton = event.getButton();
        if(pressedButton == MouseButton.SECONDARY) {
            myStage.close();
            System.exit(0);
        }
    }
}