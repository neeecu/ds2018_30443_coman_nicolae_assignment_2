import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.StageStyle;
import model.Car;
import remoteMethods.IPriceService;
import remoteMethods.ITaxService;

import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ResourceBundle;

public class ClientController implements Initializable {

    @FXML
    private TextField yearTextField;

    @FXML
    private TextField enginePowerTextField;

    @FXML
    private TextField purchasingPriceTextField;

    @FXML
    private Button taxButton;

    @FXML
    private Button priceButton;

    private Registry registry;
    private int year;
    private double price;
    private int enginePower;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            registry = LocateRegistry.getRegistry();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void priceButtonClick(ActionEvent event) throws RemoteException, NotBoundException {
        IPriceService priceService = (IPriceService) registry.lookup(IPriceService.class.getSimpleName());
        if (validateFieldsForPriceService()) {
            readFields();
            Car c = Car.builder()
                    .purchasingPrice(price)
                    .year(year)
                    .engineCapacity(enginePower)
                    .build();
            showAlert("The computed price: " + priceService.computePrice(c));
        }
    }

    @FXML
    void taxButtonClick(ActionEvent event) throws RemoteException, NotBoundException {
        ITaxService taxService = (ITaxService) registry.lookup(ITaxService.class.getSimpleName());
        if (validateFieldsForTaxService()) {
            readFields();
            Car c = Car.builder()
                    .purchasingPrice(price)
                    .year(year)
                    .engineCapacity(enginePower)
                    .build();
            showAlert("The computed tax: " + taxService.computeTax(c));
        }
    }

    private void showAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING, message, ButtonType.OK);
        alert.initStyle(StageStyle.UNDECORATED);
        alert.showAndWait();
    }

    private boolean validateFieldsForPriceService() {
        if (yearTextField.getText().isEmpty()) {
            showAlert("Please enter the fabrication year of the car !");
            return false;
        }
        if (purchasingPriceTextField.getText().isEmpty()) {
            showAlert("Please enter the purchasing price of the car !");
            return false;
        }
        if (enginePowerTextField.getText().isEmpty()) {
            showAlert("Please enter the engine capacity of the car !");
            return false;
        }
        return true;
    }

    private boolean validateFieldsForTaxService() {
        if (enginePowerTextField.getText().isEmpty()) {
            showAlert("Please enter the engine capacity of the car !");
            return false;
        }
        return true;
    }

    private void readFields() {
        String yearString = yearTextField.getText();
        String priceString = purchasingPriceTextField.getText();
        String enginePowerString = enginePowerTextField.getText();

        if (!yearString.isEmpty()) {
            year = Integer.parseInt(yearString);
        } else {
            year = 0;
        }

        if (!priceString.isEmpty()) {
            price = Double.parseDouble(priceString);
        } else {
            price = 0.0;
        }

        if (!enginePowerString.isEmpty()) {
            enginePower = Integer.parseInt(enginePowerString);
        } else {
            enginePower = 0;
        }
    }
}
