package remoteObjects;

import model.Car;
import remoteMethods.IPriceService;

import java.rmi.RemoteException;

public class PriceService implements IPriceService {
    public double computePrice(Car c) throws RemoteException, IllegalArgumentException {
        if (c == null) {
            throw new IllegalArgumentException("Car must not be null");
        } else if (c.getEngineCapacity() < 0) {
            throw new IllegalArgumentException("Engine capacity must be greater than zero");
        } else if (2018 - c.getYear() >= 7) {
            return 0.0;
        } else {
            return c.getPurchasingPrice() - ((c.getPurchasingPrice() / 7.0) * (2018 - c.getYear()));
        }
    }
}
