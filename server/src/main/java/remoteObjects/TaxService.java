package remoteObjects;

import model.Car;
import remoteMethods.ITaxService;

public class TaxService implements ITaxService {

    public double computeTax(Car c) {
        if (c == null) {
            throw new IllegalArgumentException("Car must not be null");
        } else if (c.getEngineCapacity() < 0) {
            throw new IllegalArgumentException("Engine Capacity must be greater than zero");
        } else {
            int sum = 8;
            if (c.getEngineCapacity() > 1601) sum = 18;
            if (c.getEngineCapacity() > 2001) sum = 72;
            if (c.getEngineCapacity() > 2601) sum = 144;
            if (c.getEngineCapacity() > 3001) sum = 290;
            return c.getEngineCapacity() / 200.0 * sum;
        }
    }
}
