import remoteMethods.IPriceService;
import remoteMethods.ITaxService;
import remoteObjects.PriceService;
import remoteObjects.TaxService;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerStart {

    public static void main(String[] args) throws RemoteException, UnknownHostException {

        ITaxService taxService = new TaxService();
        ITaxService taxStub = (ITaxService) UnicastRemoteObject.exportObject(taxService, 0);
        IPriceService priceService = new PriceService();
        IPriceService priceStub = (IPriceService) UnicastRemoteObject.exportObject(priceService, 0);
        Registry registry = LocateRegistry.createRegistry(1099);
         registry.rebind(ITaxService.class.getSimpleName(), taxStub);
        registry.rebind(IPriceService.class.getSimpleName(), priceStub);
    }
}
